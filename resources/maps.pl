:- dynamic([
    corona_pos/2, %posisi corona 
    rumah_pesta/2, %posisi pesta
    vaccine_pos/2, %posisi vaksin
    grid/2, %grid 1 tile

    med_scent/2, %posisi sign vaksin
    cough_sound/2, %posisi sign corona
    music_sound/2, %posisi sign pesta
    position/2, %posisi si coki jul sekarang
    visited/2,
    step/2,
    kemungkinan_corona/2, %fakta isinya list grid kemungkinan corona berdasarkan 1 sign corona
    kemungkinan_pesta/2, %fakta isinya list grid kemungkinan pesta berdasarkan 1 sign corona
    kemungkinan_vaksin/2, %fakta isinya list grid kemungkinan vaksin berdasarkan 1 sign corona
    safe/2, %posisi grid yang fix aman
    safe_not_visited/2, %grid yang safe cuma belom pernah divisit
    vaccine_count/1,
    corona_count/1
]).

antara(N1,N2,N1) :- N1 =< N2.
antara(N1,N2,R) :- N1 =< N2, NewN1 is N1+1, antara(NewN1,N2,R).

init_grids :-
    antara(0,7,X),
    antara(0,7,Y),
    assert(grid(X,Y)),
    fail.

init_map :-
    retractall(corona_pos(_,_)),
    retractall(rumah_pesta(_,_)),
    retractall(vaccine_pos(_,_)),
    retractall(grid(_,_)),
    init_map_one,    % nanti bakal dirandom map one - n, untuk sekarang map one dulu aja
    init_grids.

init_map_one :-
    assert(vaccine_count(2)),
    assert(corona_count(3)),

    assert(corona_pos(2,0)),
    assert(corona_pos(2,3)),
    assert(corona_pos(6,5)),

    assert(rumah_pesta(2,1)),
    assert(rumah_pesta(4,2)),
    assert(rumah_pesta(3,5)),

    assert(vaccine_pos(6,2)),
    assert(vaccine_pos(0,6)),
    assert(vaccine_pos(4,6)),

    % tambahan jocan
    % sign untuk vaksin (0, 6)
    assert(med_scent(0, 5)),
    assert(med_scent(1, 6)),
    assert(med_scent(0, 7)),

    % sign untuk vaksin (6, 2)
    assert(med_scent(6, 1)),
    assert(med_scent(7, 2)),
    assert(med_scent(6, 3)),
    assert(med_scent(5, 2)),

    % sign untuk vaksin (4, 6)
    assert(med_scent(4, 5)),
    assert(med_scent(5, 6)),
    assert(med_scent(4, 7)),
    assert(med_scent(3, 6)),
    
    % sign untuk corona_pos(2, 0)
    assert(cough_sound(3, 0)),
    assert(cough_sound(1, 0)),

    % sign untuk corona_pos(2, 3)
    assert(cough_sound(2, 2)),
    assert(cough_sound(3, 3)),
    assert(cough_sound(2, 4)),
    assert(cough_sound(1, 3)),

    % sign untuk corona_pos(6, 5)
    assert(cough_sound(6, 4)),
    assert(cough_sound(7, 5)),
    assert(cough_sound(6, 6)),
    assert(cough_sound(5, 5)),

    % sign untuk rumah_pesta(2, 1)
    assert(music_sound(3, 1)),
    assert(music_sound(2, 2)),
    assert(music_sound(1, 1)),

    % sign untuk corona_pos(4, 2)
    assert(music_sound(4, 1)),
    assert(music_sound(5, 2)),
    assert(music_sound(4, 3)),
    assert(music_sound(3, 2)),

    % sign untuk corona_pos(3, 5)
    assert(music_sound(3, 4)),
    assert(music_sound(4, 5)),
    assert(music_sound(3, 6)),
    assert(music_sound(2, 5)),

    assert(safe(0,0)),
    assert(safe(0,1)),
    assert(safe(1,0)),
    
    assert(safe_not_visited(0, 1)),
    assert(safe_not_visited(1, 0)),

    assert(position(0,0)),

    assert(visited(0,0)).

no_obstacle(X,Y) :- grid(X,Y), !, \+corona_pos(X,Y).
no_obstacle(X,Y) :- grid(X,Y), \+rumah_pesta(X,Y).

cari_path(Vaccine_Count) :-
    antara(0,7,X),
    antara(0,7,Y),
    write('Currently at Position ('),
    write(X),
    write(', '),
    write(Y),
    write(')\n'),
    no_obstacle(X,Y),
    write('Current position is safe \n'),
    Vaccine_Count2 = VaccineCount,
    cek_vaccine(X, Y, Found),
    Vaccine_Count2 is Vaccine_Count + Found,
    write(VaccineCount2),
    write('\n\n'),
    !.


cek_vaccine(X, Y, Found):-
    vaccine_pos(X,Y),
    write('Found vaccine at pos ('),
    write(X),
    write(', '),
    write(Y),
    write(')\n'),
    retract(vaccine_pos(X,Y)),
    write('\n\n'),
    Found = 1,
    fail.

%buat jalan
move(NextX, NextY) :-
    position(CurrentX, CurrentY),
    PrevX = CurrentX,
    PrevY = CurrentY,
    checkVisit(NextX, NextY),
    %addVisited(PrevX, PrevY),
    remove_safe_not_visited(NextX, NextY),
    retract(position(PrevX, PrevY)),
    assert(position(NextX, NextY)),

    adjacent((NextX, NextY), 4, Adjacents),
    filter_by_not_safe(Adjacents, FilteredList),
    generate_kemungkinan((NextX, NextY), FilteredList),
    % inferAll,
    infer(NextX, NextY),
    generalize(NextX, NextY),
    corona_count(C),
    check_win(C),
    stepped_on_vaksin(NextX,NextY).

stepped_on_vaksin(X,Y) :-
    vaccine_pos(X,Y),!,
    update_vaksin(grid(X,Y)).

stepped_on_vaksin(X,Y) :-
    true.

%addVisited(X, Y) :- 
%    visited(X, Y), !.
%addVisited(X, Y) :-
%    assert(visited(X, Y)).

remove_safe_not_visited(X, Y) :- 
    safe_not_visited(X, Y), 
    retract(safe_not_visited(X, Y)), !.
remove_safe_not_visited(X, Y) :- true.

%filter adjacent buang yang udh safe
filter_by_not_safe([grid(X, Y)], FilteredList) :- safe(X, Y), !, FilteredList = []. %basecase_kalo_udh_safe
filter_by_not_safe([grid(X, Y)], FilteredList) :- !, FilteredList = [grid(X, Y)]. %basecase_kalo_blom_safe
filter_by_not_safe([grid(X, Y) | Tail], FilteredList) :- %kalo_udh_safe_ya_buang
    safe(X, Y), !, 
    filter_by_not_safe(Tail, NewFilteredList),
    FilteredList = NewFilteredList.
filter_by_not_safe([grid(X, Y) | Tail], FilteredList) :- %kalo_blom_safe_masukin_ke_filtered_list
    filter_by_not_safe(Tail, NewFilteredList),
    append([grid(X, Y)], NewFilteredList, FilteredList).

%generate_predikat_kemungkinan_bahaya_dan_vaksin

% base case kalau hanya ada 1 kemungkinan grid fix disitu posisi
generate_from_sign(X, Y, [grid(X1,Y1)], Flag) :- 
    cough_sound(X, Y),
    !,
    assert(corona_pos(X1,Y1)),
    write('Corona Position found at ('),
    write(X1),
    write(', '),
    write(Y1),
    write(')'),
    Flag is 1.
generate_from_sign(X, Y, ListAdjacent, Flag) :- 
    cough_sound(X, Y),
    assert(ada_cough_sound(X,Y)),
    assert(kemungkinan_corona(cough_sound(X, Y), ListAdjacent)),
    Flag is 1.

generate_from_sign(X, Y, [grid(X1,Y1)], Flag) :- 
    music_sound(X, Y),
    !,
    assert(rumah_pesta(X1,Y1)),
    write('Rumah Pesta Position found at ('),
    write(X),
    write(', '),
    write(Y),
    write(')'),
    Flag is 1.
generate_from_sign(X, Y, ListAdjacent, Flag) :- 
    music_sound(X, Y),
    assert(kemungkinan_pesta(music_sound(X, Y), ListAdjacent)),
    Flag is 1.

generate_from_sign(X, Y, [grid(X1,Y1)], Flag) :- 
    med_scent(X, Y),
    !,
    assert(vaccine_pos(X1,Y1)),
    write('Vaccine Position found at ('),
    write(X),
    write(', '),
    write(Y),
    write(')'),
    Flag is 1.
generate_from_sign(X, Y, ListAdjacent, Flag) :- 
    med_scent(X, Y),
    assert(kemungkinan_vaksin(med_scent(X, Y), ListAdjacent)),
    Flag is 1.

%generate_predikat_yang_safe
generate_safe([grid(X, Y)]) :- 
    assert(safe(X, Y)), 
    assert(safe_not_visited(X, Y)),
    update_seluruh_kemungkinan(grid(X, Y)), 
    true, !.
generate_safe([grid(X, Y) | Tails]) :-
    assert(safe(X, Y)), 
    assert(safe_not_visited(X, Y)),
    update_seluruh_kemungkinan(grid(X, Y)),
    generate_safe(Tails).

%generate_predikat_kemungkinan
generate_kemungkinan((_, _), []) :- !.
generate_kemungkinan((X, Y), ListAdjacent) :- 
    findall(Flag, generate_from_sign(X, Y, ListAdjacent, Flag), ListFlag),
    length(ListFlag, Length),
    Length > 0, !.
generate_kemungkinan((X, Y), ListAdjacent) :- 
    generate_safe(ListAdjacent).

%buat generate adjacentnya apa aja (urutannya atas kanan bawah kiri)
adjacent((_,_), 0, []).
adjacent((X,Y), 1, Adjacents) :- X = 0, !,  adjacent((X, Y), 0, Adjacents).
adjacent((X,Y), 1, Adjacents) :- adjacent((X, Y), 0, NewAdjacents), NewX is X - 1, LeftAdjacent = grid(NewX, Y), append([LeftAdjacent], NewAdjacents, Adjacents).
adjacent((X,Y), 2, Adjacents) :- Y = 7, !, adjacent((X, Y), 1, Adjacents).
adjacent((X,Y), 2, Adjacents) :- adjacent((X, Y), 1, NewAdjacents), NewY is Y + 1, BottomAdjacent = grid(X, NewY), append([BottomAdjacent], NewAdjacents, Adjacents).
adjacent((X,Y), 3, Adjacents) :- X = 7, !, adjacent((X, Y), 2, Adjacents).
adjacent((X,Y), 3, Adjacents) :- adjacent((X, Y), 2, NewAdjacents), NewX is X + 1, RightAdjacent = grid(NewX, Y), append([RightAdjacent], NewAdjacents, Adjacents).
adjacent((X,Y), 4, Adjacents) :- Y = 0, !, adjacent((X, Y), 3, Adjacents).
adjacent((X,Y), 4, Adjacents) :- adjacent((X, Y), 3, NewAdjacents), NewY is Y - 1, TopAdjacent = grid(X, NewY), append([TopAdjacent], NewAdjacents, Adjacents).

%kemungkinan_vaksin(med_scent(1, 0), [grid(2, 0), grid(1, 1)]).
delete_kemungkinan_vaksin([]):-
    !,true.
delete_kemungkinan_vaksin([grid(X,Y)|Tail]):-
    kemungkinan_vaksin(med_scent(X,Y),Grid),!,
    retract(kemungkinan_vaksin(med_scent(X,Y),Grid)),
    delete_kemungkinan_vaksin(Tail).
delete_kemungkinan_vaksin([grid(X,Y)|Tail]):-
    delete_kemungkinan_vaksin(Tail).

%buat update kemungkinan corona sama pesta (untuk sekarang)
update_seluruh_kemungkinan(Grid) :- 
    findall(X, cari_kemungkinan_corona(Grid, X), ListKemungkinanCorona),
    update_kemungkinan(Grid, ListKemungkinanCorona, 1),
    findall(X, cari_kemungkinan_pesta(Grid, X), ListKemungkinanPesta),
    update_kemungkinan(Grid, ListKemungkinanPesta, 2),
    true.

%buat cari 1 fakta kemungkinan corona yang dimana Grid bagian dari ListGrid
cari_kemungkinan_corona(Grid, X) :-  
    kemungkinan_corona(Cough, ListGrid),
    member(Grid, ListGrid),
    X = kemungkinan_corona(Cough, ListGrid).

%buat cari 1 fakta kemungkinan pesta yang dimana Grid bagian dari ListGrid
cari_kemungkinan_pesta(Grid, X) :-  
    kemungkinan_pesta(MusicSound, ListGrid),
    member(Grid, ListGrid),
    X = kemungkinan_pesta(MusicSound, ListGrid).

%buat update kemungkinan(1 -> kemungkinan corona, 2 -> kemungkinan pesta)
update_kemungkinan(_, [], 1) :- 
    !, true.
update_kemungkinan(Grid, [kemungkinan_corona(Cough, ListKemungkinan)], 1) :- 
    !, retract(kemungkinan_corona(Cough, ListKemungkinan)),
    delete(ListKemungkinan, Grid, FilteredKemungkinan),
    assert(kemungkinan_corona(Cough, FilteredKemungkinan)).
update_kemungkinan(Grid, [kemungkinan_corona(Cough, ListKemungkinan) | Tail], 1) :- 
    retract(kemungkinan_corona(Cough, ListKemungkinan)),
    delete(ListKemungkinan, Grid, FilteredKemungkinan),
    assert(kemungkinan_corona(Cough, FilteredKemungkinan)),
    update_kemungkinan(Grid, Tail, 1).
update_kemungkinan(_, [], 2) :- 
    !, true.
update_kemungkinan(Grid, [kemungkinan_pesta(MusicSound, ListKemungkinan)], 2) :- 
    !, retract(kemungkinan_pesta(MusicSound, ListKemungkinan)),
    delete(ListKemungkinan, Grid, FilteredKemungkinan),
    assert(kemungkinan_pesta(MusicSound, FilteredKemungkinan)).
update_kemungkinan(Grid, [kemungkinan_pesta(MusicSound, ListKemungkinan) | Tail], 2) :- 
    retract(kemungkinan_pesta(MusicSound, ListKemungkinan)),
    delete(ListKemungkinan, Grid, FilteredKemungkinan),
    assert(kemungkinan_pesta(MusicSound, FilteredKemungkinan)),
    update_kemungkinan(Grid, Tail, 2).

update_vaksin(grid(X,Y)) :-
    retract(vaccine_pos(X,Y)),
    adjacent((X, Y), 4, Adjacents),
    delete_kemungkinan_vaksin(Adjacents),
    vaccine_count(Count),
    Count2 is Count + 1,
    retract(vaccine_count(Count)),
    assert(vaccine_count(Count2)).

update_corona(grid(X,Y)) :-
    retract(corona_pos(X,Y)),
    adjacent((X, Y), 4, Adjacents),
    delete_kemungkinan_corona(Adjacents),
    corona_count(Count),
    Count2 is Count - 1,
    retract(corona_count(Count)),
    assert(corona_count(Count2)).

delete_kemungkinan_corona([]):-
    !,true.
delete_kemungkinan_corona([grid(X,Y)|Tail]):-
    kemungkinan_corona(cough_sound(X,Y),Grid),!,
    retract(kemungkinan_corona(cough_sound(X,Y),Grid)),
    delete_kemungkinan_corona(Tail).
delete_kemungkinan_corona([grid(X,Y)|Tail]):-
    delete_kemungkinan_corona(Tail).

%kemungkinan_corona(cough_sound(1, 0), [grid(2, 0), grid(1, 1)]).

% ide
% infer suatu tile bahaya = dikelilingi sinyal (4 kalo ga corner case), 
% masing masing dari sinyal pada KemungkinanListnya menunjuk tile tsb
% corner case = (0,Y) (X,0) (0,7) (7,0) (7,7)
% ide next ketika suatu tile udah safe maka KemungkinanList dihilangin tile tsb
% kalo KemugnkinanList hanya berisi 1 tile maka tile tsb adalah bahaya/vaksin
% kalo ada corona / ruma pesta ditemuin, delete kemungkinan kalo misal ada lebih dari 1

%case 4 known corona
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    kemungkinan_corona(cough_sound(Xr,Y), ListR),
    kemungkinan_corona(cough_sound(Xl,Y), ListL),
    kemungkinan_corona(cough_sound(X,Yu), ListU),
    kemungkinan_corona(cough_sound(X,Yb), ListB),
    member(grid(X,Y), ListR),
    member(grid(X,Y), ListL),
    member(grid(X,Y), ListU),
    member(grid(X,Y), ListB),
    assert(corona_pos(X,Y)),
    write('Corona Position found at ('),
    write(X),
    write(', '),
    write(Y),
    write(')'),
    !.

% case left dan top known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_corona(cough_sound(Xl,Y), ListL),
    kemungkinan_corona(cough_sound(X,Yu), ListU),
    member(grid(Xl,Yu), ListL),
    member(grid(Xl,Yu), ListU),
    assert(corona_pos(Xl,Yu)),
    write('Corona Position found at ('),
    write(Xl),
    write(', '),
    write(Yu),
    write(')'),
    !.

% case right dan bot known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_corona(cough_sound(Xr,Y), ListL),
    kemungkinan_corona(cough_sound(X,Yb), ListU),
    member(grid(Xr,Yb), ListL),
    member(grid(Xr,Yb), ListU),
    assert(corona_pos(Xr,Yb)),
    write('Corona Position found at ('),
    write(Xr),
    write(', '),
    write(Yb),
    write(')'),
    !.

% case right dan top known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_corona(cough_sound(Xr,Y), ListL),
    kemungkinan_corona(cough_sound(X,Yu), ListU),
    member(grid(Xr,Yu), ListL),
    member(grid(Xr,Yu), ListU),
    assert(corona_pos(Xr,Yu)),
    write('Corona Position found at ('),
    write(Xr),
    write(', '),
    write(Yu),
    write(')'),
    !.

% case left dan bot known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_corona(cough_sound(Xl,Y), ListL),
    kemungkinan_corona(cough_sound(X,Yb), ListU),
    member(grid(Xl,Yb), ListL),
    member(grid(Xl,Yb), ListU),
    assert(corona_pos(Xl,Yb)),
    write('Corona Position found at ('),
    write(Xl),
    write(', '),
    write(Yb),
    write(')'),
    !.

% case for rumah pesta
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    kemungkinan_pesta(music_sound(Xr,Y), ListR),
    kemungkinan_pesta(music_sound(Xl,Y), ListL),
    kemungkinan_pesta(music_sound(X,Yu), ListU),
    kemungkinan_pesta(music_sound(X,Yb), ListB),
    member(grid(X,Y), ListR),
    member(grid(X,Y), ListL),
    member(grid(X,Y), ListU),
    member(grid(X,Y), ListB),
    assert(rumah_pesta(X,Y)),
    write('Rumah Pesta Position found at ('),
    write(X),
    write(', '),
    write(Y),
    write(')'),
    !.

% case left dan top known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_pesta(music_sound(Xl,Y), ListL),
    kemungkinan_pesta(music_sound(X,Yu), ListU),
    member(grid(Xl,Yu), ListL),
    member(grid(Xl,Yu), ListU),
    assert(rumah_pesta(Xl,Yu)),
    write('Rumah Pesta Position found at ('),
    write(Xl),
    write(', '),
    write(Yu),
    write(')'),
    !.

% case right dan bot known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_pesta(music_sound(Xr,Y), ListL),
    kemungkinan_pesta(music_sound(X,Yb), ListU),
    member(grid(Xr,Yb), ListL),
    member(grid(Xr,Yb), ListU),
    assert(rumah_pesta(Xr,Yb)),
    write('Rumah Pesta Position found at ('),
    write(Xr),
    write(', '),
    write(Yb),
    write(')'),
    !.

% case right dan top known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_pesta(music_sound(Xr,Y), ListL),
    kemungkinan_pesta(music_sound(X,Yu), ListU),
    member(grid(Xr,Yu), ListL),
    member(grid(Xr,Yu), ListU),
    assert(rumah_pesta(Xr,Yu)),
    write('Rumah Pesta Position found at ('),
    write(Xr),
    write(', '),
    write(Yu),
    write(')'),
    !.

% case left dan bot known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_pesta(music_sound(Xl,Y), ListL),
    kemungkinan_pesta(music_sound(X,Yb), ListU),
    member(grid(Xl,Yb), ListL),
    member(grid(Xl,Yb), ListU),
    assert(rumah_pesta(Xl,Yb)),
    write('Rumah Pesta Position found at ('),
    write(Xl),
    write(', '),
    write(Yb),
    write(')'),
    !.


% case for vaksin
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    kemungkinan_vaksin(med_scent(Xr,Y), ListR),
    kemungkinan_vaksin(med_scent(Xl,Y), ListL),
    kemungkinan_vaksin(med_scent(X,Yu), ListU),
    kemungkinan_vaksin(med_scent(X,Yb), ListB),
    member(grid(X,Y), ListR),
    member(grid(X,Y), ListL),
    member(grid(X,Y), ListU),
    member(grid(X,Y), ListB),
    assert(vaccine_pos(X,Y)),
    write('Vaksin Position found at ('),
    write(X),
    write(', '),
    write(Y),
    write(')'),
    !.

% case left dan top known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_vaksin(med_scent(Xl,Y), ListL),
    kemungkinan_vaksin(med_scent(X,Yu), ListU),
    member(grid(Xl,Yu), ListL),
    member(grid(Xl,Yu), ListU),
    assert(vaccine_pos(Xl,Yu)),
    write('Vaksin Position found at ('),
    write(Xl),
    write(', '),
    write(Yu),
    write(')'),
    !.

% case right dan bot known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_vaksin(med_scent(Xr,Y), ListL),
    kemungkinan_vaksin(med_scent(X,Yb), ListU),
    member(grid(Xr,Yb), ListL),
    member(grid(Xr,Yb), ListU),
    assert(vaccine_pos(Xr,Yb)),
    write('Vaksin Position found at ('),
    write(Xr),
    write(', '),
    write(Yb),
    write(')'),
    !.

% case right dan top known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_vaksin(med_scent(Xr,Y), ListL),
    kemungkinan_vaksin(med_scent(X,Yu), ListU),
    member(grid(Xr,Yu), ListL),
    member(grid(Xr,Yu), ListU),
    assert(vaccine_pos(Xr,Yu)),
    write('Vaksin Position found at ('),
    write(Xr),
    write(', '),
    write(Yu),
    write(')'),
    !.

% case left dan bot known, current safe
infer(X,Y) :-
    grid(X,Y),
    Xr is X+1,
    Xl is X-1,
    Yu is Y-1,
    Yb is Y+1,
    safe(X,Y),
    kemungkinan_vaksin(med_scent(Xl,Y), ListL),
    kemungkinan_vaksin(med_scent(X,Yb), ListU),
    member(grid(Xl,Yb), ListL),
    member(grid(Xl,Yb), ListU),
    assert(vaccine_pos(Xl,Yb)),
    write('Vaksin Position found at ('),
    write(Xl),
    write(', '),
    write(Yb),
    write(')'),
    !.

% case kalo memang belum cukup evidence
infer(X,Y).

checkVisit(X,Y) :-
    \+visited(X,Y),
    assert(visited(X,Y)),
    !.

checkVisit(X,Y).

% inferAll :-
%     findall()

generalize(X,Y) :-
    \+corona_pos(X,Y),
    \+rumah_pesta(X,Y),
    !.

generalize(X,Y) :-
    rumah_pesta(X,Y),
    !,
    write('Brought to rumah pesta, now you infected, Game Over'),
    fail.

generalize(X,Y) :-
    corona_pos(X,Y),
    vaccine_count(V),
    V > 0,
    !,
    V2 is V - 1,
    write('Saved person in grid('),
    write(X),
    write(', '),
    write(Y),
    write(')\n'),
    write('Vaccine Remaining : '),
    write(V2),
    write('\n'),
    retract(vaccine_count(V)),
    assert(vaccine_count(V2)),
    update_corona(grid(X,Y)),
    corona_count(C),
    write('Corona Remaining : '),
    write(C),
    write('\n').

generalize(X,Y) :-
    corona_pos(X,Y),
    !,
    write('Oh no you are infected by corona virus and dead'),
    fail.

generalize(X,Y).

check_win(X) :-
    X =:= 0,
    !,
    write('You succedeed helping every corona infected people!'),
    fail.

check_win(X).